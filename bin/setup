#!/bin/bash

# Exit when any command fails.
set -e

if ! command -v composer &> /dev/null 2>&1
then
  echo "Composer executable could not be found"
  exit
fi
# Check composer version and constraints
CurrV=$(composer --version)
ExpecV='Composer version 2.2.0'
printf -v versions '%s\n%s' "$CurrV" "$ExpecV"
if [[ $versions = "$(sort -V <<< "$versions")" ]]; then
  SUPPORT_ALLOW_PLUGINS=0
else
  SUPPORT_ALLOW_PLUGINS=1
fi

# Read environment variables if present.
if [[ -f .composer-plugin.env ]]; then
  export $(cat .composer-plugin.env | xargs) > /dev/null 2>&1
fi

readArgument() {
  PROMPT=$1
  DEFAULT=$2
  VARIABLE=$3

  # Verify, if the given env variable is already set.
  VALUE=$(printenv | grep $VARIABLE= | cut -d'=' -f 2)
  if [[ "x$VALUE" == "x" ]]; then
    if [[ "x$NONINTERACTIVE" == "x" ]]; then
      # If in interactive mode, ask for user input for the env variable.
      read -r -p "${PROMPT}? [${DEFAULT}] " INPUT
      if [[ "x$INPUT" == "x" ]]; then
        # If no input given, fall back to default value.
        export VALUE=$DEFAULT
      else
        export VALUE=$INPUT
      fi
    else
      # In non-interactive mode use default value.
      export VALUE=$DEFAULT
    fi
  fi
  export $VARIABLE=$VALUE
  echo "$VARIABLE=${VALUE}" >>.composer-plugin.env
}

COMPOSER=composer.spoons.json
export COMPOSER

readArgument "Drupal core constraint" "^9" DRUPAL_CORE_CONSTRAINT
readArgument "Composer plugin constraint" "^2" COMPOSER_PLUGIN_CONSTRAINT
# The following arguments are currently turned off, as the local process doesn't need them.
#readArgument "PHP version" "7.4" PHP_TAG
#readArgument "Database driver" "mysql" DB_DRIVER
#if [[ "$DB_DRIVER" == "mysql" ]]; then readArgument "MariaDB version" "10.3" MARIADB_TAG; fi
#if [[ "$DB_DRIVER" == "postgres" ]]; then readArgument "Postgres version" "10.5" POSTGRES_TAG; fi

# Remove duplicates from environment variable store.
env -i $(cat .composer-plugin.env | xargs) >.composer-plugin.env

# Prepare COMPOSER file.
if [[ ! -f $COMPOSER ]]; then
  composer init --no-interaction --quiet --name=drupalspoons/template --stability=dev
fi
# Prepare allowed plugins.
if [[ $SUPPORT_ALLOW_PLUGINS -eq 1 ]]; then
  composer config allow-plugins.composer/installers true
  composer config allow-plugins.cweagans/composer-patches true
  composer config allow-plugins.dealerdirect/phpcodesniffer-composer-installer true
  composer config allow-plugins.drupal/core-composer-scaffold true
  composer config allow-plugins.drupalspoons/composer-plugin true
fi
# Accept a constraint for composer-plugin.
echo -e "\n\n\nInstalling composer-plugin"
composer require --dev --no-interaction drupalspoons/composer-plugin:$COMPOSER_PLUGIN_CONSTRAINT
echo -e "\n\n\nPreparing $COMPOSER"
composer drupalspoons:composer-json
echo -e "\nConfiguring project for local tests"
composer drupalspoons:configure
echo -e "\nInstalling dependencies"
composer update --prefer-stable --no-interaction --no-progress
echo -e "\nConditionally installing Prophecy"
composer drupalspoons:prophecy

echo -e "\n\n\n==============================================================="
echo "Successfully installed DrupalCI for local usage."
echo "Use https://direnv.net/ or similar to configure your local environment."
echo "See the new .envrc in your project root."
echo -e "\n\nUsage:"
echo "     composer [script]"
echo -e "\n\nwhere [script] should be replaced by one of the"
echo "available scripts for testing."
echo -e "\n\nExamples:"
echo "Run PHPUnit:            composer unit"
echo "Run PHPCS:              composer phpcs"
echo "Rebuild codebase:       composer drupalspoons:rebuild"
echo "==============================================================="

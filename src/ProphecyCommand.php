<?php

namespace SpoonsPlugin;

use Composer\Command\BaseCommand;
use Composer\InstalledVersions;
use Composer\Semver\Comparator;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProphecyCommand extends BaseCommand {

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this->setName('drupalspoons:prophecy');
    $this->setDescription('Conditionally install phpspec/prophecy-phpunit package.');
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    if (Comparator::greaterThanOrEqualTo(InstalledVersions::getVersion('phpunit/phpunit'), '9.0.0')) {
      passthru('composer require --prefer-stable -n --dev phpspec/prophecy-phpunit:^2', $exit_code);
      return $exit_code;
    }
    return 0;
  }

}
